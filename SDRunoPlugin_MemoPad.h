#pragma once

#include <thread>
#include <mutex>
#include <atomic>
#include <iunoplugincontroller.h>
#include <iunoplugin.h>
#include <iunostreamobserver.h>
#include <iunoaudioobserver.h>
#include <iunoaudioprocessor.h>
#include <iunostreamobserver.h>
#include <iunoannotator.h>

#include "SDRunoPlugin_MemoPadUi.h"
#include "MemoController.h"

class SDRunoPlugin_MemoPad : public IUnoPlugin, public IUnoAnnotator
{

public:
	
	SDRunoPlugin_MemoPad(IUnoPluginController& controller);
	virtual ~SDRunoPlugin_MemoPad();

	// TODO: change the plugin title here
	virtual const char* GetPluginName() const override { return "SDRuno Memo pad"; }

	// IUnoPlugin
	virtual void HandleEvent(const UnoEvent& ev) override;

	virtual void AnnotatorProcess(std::vector<IUnoAnnotatorItem>& items) override;

	void StartAnnotator();
	void StopAnnotator();

	//Memo Pad methods
	void ClickPush();
	void ClickPop();
	void ClickFrequncyLabel(int i);
	void DblClickFrequncyLabel(int i);
	void ClickClear();

private:
	
	MemoController memoController;
	void WorkerFunction();
	std::thread* m_worker;
	std::mutex m_lock;
	SDRunoPlugin_MemoPadUi m_form;
	
};