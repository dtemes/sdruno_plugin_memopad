#pragma once

#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/timer.hpp>
#include <iunoplugin.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <iunoplugincontroller.h>
#include "SDRunoPlugin_MemoPadForm.h"

// Forward reference
class SDRunoPlugin_MemoPad;

class SDRunoPlugin_MemoPadUi
{
public:

	SDRunoPlugin_MemoPadUi(SDRunoPlugin_MemoPad& parent, IUnoPluginController& controller);
	~SDRunoPlugin_MemoPadUi();

	void HandleEvent(const UnoEvent& evt);
	void FormClosed();

	void ShowUi();

	int LoadX();
	int LoadY();

	//Memo Pad methods
	void ClickPush();
	void ClickPop();
	void ClickClear();
	void ClickFrequncyLabel(int i);
	void DblClickFrequncyLabel(int i);

	void SetFrequencyLabelText(int position, std::string text);
	
private:
	
	SDRunoPlugin_MemoPad & m_parent;
	std::thread m_thread;
	std::shared_ptr<SDRunoPlugin_MemoPadForm> m_form;
	void SaveLocation();

	bool m_started;

	std::mutex m_lock;

	IUnoPluginController & m_controller;
};
