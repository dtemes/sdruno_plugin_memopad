#include <sstream>
#include <unoevent.h>
#include <iunoplugincontroller.h>
#include <vector>
#include <sstream>
#include <chrono>

#include "SDRunoPlugin_MemoPad.h"
#include "SDRunoPlugin_MemoPadUi.h"


SDRunoPlugin_MemoPad::SDRunoPlugin_MemoPad(IUnoPluginController& controller) :
	IUnoPlugin(controller),
	m_form(*this, controller),
	m_worker(nullptr)
{
	StartAnnotator();
}

SDRunoPlugin_MemoPad::~SDRunoPlugin_MemoPad()
{	
	StopAnnotator();
}

void SDRunoPlugin_MemoPad::HandleEvent(const UnoEvent& ev)
{
	m_form.HandleEvent(ev);	
}

void SDRunoPlugin_MemoPad::WorkerFunction()
{
	// Worker Function Code Goes Here
}

void SDRunoPlugin_MemoPad::StartAnnotator()
{
	m_controller.RegisterAnnotator(this);
}

void SDRunoPlugin_MemoPad::StopAnnotator()
{
	m_controller.UnregisterAnnotator(this);
}

void SDRunoPlugin_MemoPad::AnnotatorProcess(std::vector<IUnoAnnotatorItem>& items)
{
	const uint32_t colors[] = {
		0x00ff0000, // red
		0x00ffff00, // yellow
		0x00ff00ff, // purple
		0x00aaff00, // green
		0x0030a0ff, // light blue
		0x00ffffff  // white
	};

	
	for (int i = 0; i < memoController.GetNumberOfMemories(); i++)
	{

		long long freq = memoController.GetFrequency(i);
		if (freq != 0)
		{
			IUnoAnnotatorItem im;
			im.frequency = freq;
			im.text = std::to_string(i);
			im.power = -70;
			im.rgb = colors[i % sizeof(colors)];
			im.style = IUnoAnnotatorStyle::AnnotatorStyleMarkerAndLine;
			items.push_back(im);
		}
		
	}

		
	
		
}

void SDRunoPlugin_MemoPad::ClickPush()
{
	long long freq = m_controller.GetVfoFrequency(0);
	int pos = memoController.Push(freq);

	std::ostringstream ss;
	ss.imbue(std::locale(""));
	ss << freq;
	
	m_form.SetFrequencyLabelText(pos, ss.str());
		
}

void SDRunoPlugin_MemoPad::ClickPop()
{
	long long freq = memoController.Pop();
	m_controller.SetVfoFrequency(0, freq);
}

void SDRunoPlugin_MemoPad::ClickClear()
{
	memoController.ClearAll();
	for (int i = 0; i < NUM_MEMORIES; i++)
	{
		m_form.SetFrequencyLabelText(i, "-");
	}
}

void SDRunoPlugin_MemoPad::ClickFrequncyLabel(int i)
{
	long long freq = memoController.GetFrequency(i);
	memoController.SetPosition(i);
	m_controller.SetVfoFrequency(0, freq);
	
}

void SDRunoPlugin_MemoPad::DblClickFrequncyLabel(int i)
{
	memoController.ClearPosition(i);
	m_form.SetFrequencyLabelText(i, "-");

}
