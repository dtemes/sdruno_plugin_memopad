#pragma once

#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/listbox.hpp>
#include <nana/gui/widgets/slider.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/combox.hpp>
#include <nana/gui/timer.hpp>
#include <nana/gui/widgets/picture.hpp>
#include <nana/gui/filebox.hpp>
#include <nana/gui/dragger.hpp>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <iunoplugincontroller.h>
#include "MemoController.h"

// Shouldn't need to change these
#define topBarHeight (27)
#define bottomBarHeight (8)
#define sideBorderWidth (8)

// TODO: Change these numbers to the height and width of your form
#define formWidth (297)
#define formHeight (260)

class SDRunoPlugin_MemoPadUi;

class SDRunoPlugin_MemoPadForm : public nana::form
{

public:

	SDRunoPlugin_MemoPadForm(SDRunoPlugin_MemoPadUi& parent, IUnoPluginController& controller);		
	~SDRunoPlugin_MemoPadForm();
	
	void SetFrequencyLabelText (int position, std::string text);
	void Run();
	
private:

	void Setup();

	// The following is to set up the panel graphic to look like a standard SDRuno panel
	nana::picture bg_border{ *this, nana::rectangle(0, 0, formWidth, formHeight) };
	nana::picture bg_inner{ bg_border, nana::rectangle(sideBorderWidth, topBarHeight, formWidth - (2 * sideBorderWidth), formHeight - topBarHeight - bottomBarHeight) };
	nana::picture header_bar{ *this, true };
	nana::label title_bar_label{ *this, true };
	nana::dragger form_dragger;
	nana::label form_drag_label{ *this, nana::rectangle(0, 0, formWidth, formHeight) };
	nana::paint::image img_min_normal;
	nana::paint::image img_min_down;
	nana::paint::image img_close_normal;
	nana::paint::image img_close_down;
	nana::paint::image img_header;
	nana::picture close_button{ *this, nana::rectangle(0, 0, 20, 15) };
	nana::picture min_button{ *this, nana::rectangle(0, 0, 20, 15) };
	nana::label versionLbl{ *this, nana::rectangle(formWidth - 40, formHeight - 30, 30, 20) };

	// Uncomment the following 5 lines if you want a SETT button and panel
	nana::paint::image img_sett_normal;
	nana::paint::image img_sett_down;
	nana::picture sett_button{ *this, nana::rectangle(0, 0, 40, 15) };
	void SettingsButton_Click();
	void SettingsDialog_Closed();

	// TODO: Now add your UI controls here
	nana::button pushBtn{ *this, nana::rectangle (20 * 1,          40, 65, 30) };
	nana::button popBtn{ *this, nana::rectangle  (20 * 2 + 65,     40, 65, 30) };
	nana::button clearBtn{ *this, nana::rectangle(20 * 3 + 65 * 2, 40, 65, 30) };

	nana::label *lblFrequency[NUM_MEMORIES];
	nana::button *btnFrequency[NUM_MEMORIES];

	SDRunoPlugin_MemoPadUi & m_parent;
	IUnoPluginController & m_controller;
};
