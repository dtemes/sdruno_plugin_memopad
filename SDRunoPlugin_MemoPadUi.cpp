#include <sstream>
#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/listbox.hpp>
#include <nana/gui/widgets/slider.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/timer.hpp>
#include <unoevent.h>

#include "SDRunoPlugin_MemoPad.h"
#include "SDRunoPlugin_MemoPadUi.h"
#include "SDRunoPlugin_MemoPadForm.h"

// Ui constructor - load the Ui control into a thread
SDRunoPlugin_MemoPadUi::SDRunoPlugin_MemoPadUi(SDRunoPlugin_MemoPad& parent, IUnoPluginController& controller) :
	m_parent(parent),
	m_form(nullptr),
	m_controller(controller)
{
	m_thread = std::thread(&SDRunoPlugin_MemoPadUi::ShowUi, this);
}

// Ui destructor (the nana::API::exit_all();) is required if using Nana UI library
SDRunoPlugin_MemoPadUi::~SDRunoPlugin_MemoPadUi()
{	
	nana::API::exit_all();
	m_thread.join();	
}

// Show and execute the form
void SDRunoPlugin_MemoPadUi::ShowUi()
{	
	m_lock.lock();
	m_form = std::make_shared<SDRunoPlugin_MemoPadForm>(*this, m_controller);
	m_lock.unlock();

	m_form->Run();
}

// Load X from the ini file (if exists)
int SDRunoPlugin_MemoPadUi::LoadX()
{
	std::string tmp;
	m_controller.GetConfigurationKey("MemoPad.X", tmp);
	if (tmp.empty())
	{
		return -1;
	}
	return stoi(tmp);
}

// Load Y from the ini file (if exists)
int SDRunoPlugin_MemoPadUi::LoadY()
{
	std::string tmp;
	m_controller.GetConfigurationKey("MemoPad.Y", tmp);
	if (tmp.empty())
	{
		return -1;
	}
	return stoi(tmp);
}

void SDRunoPlugin_MemoPadUi::SaveLocation()
{
	std::lock_guard<std::mutex> l(m_lock);
	nana::point position = m_form->pos();
	m_controller.SetConfigurationKey("MemoPad.X", std::to_string(position.x));
	m_controller.SetConfigurationKey("MemoPad.Y", std::to_string(position.y));
}

void SDRunoPlugin_MemoPadUi::ClickPush()
{
	m_parent.ClickPush();
}

void SDRunoPlugin_MemoPadUi::ClickPop()
{
	m_parent.ClickPop();
}

void SDRunoPlugin_MemoPadUi::ClickClear()
{
	m_parent.ClickClear();
}

void SDRunoPlugin_MemoPadUi::ClickFrequncyLabel(int i)
{
	m_parent.ClickFrequncyLabel(i);
}

void SDRunoPlugin_MemoPadUi::DblClickFrequncyLabel(int i)
{
	m_parent.DblClickFrequncyLabel(i);
}

void SDRunoPlugin_MemoPadUi::SetFrequencyLabelText(int position, std::string text)
{
	m_form->SetFrequencyLabelText(position, text);
}


// Handle events from SDRuno
// TODO: code what to do when receiving relevant events
void SDRunoPlugin_MemoPadUi::HandleEvent(const UnoEvent& ev)
{
	switch (ev.GetType())
	{
	case UnoEvent::StreamingStarted:
		break;

	case UnoEvent::StreamingStopped:
		break;

	case UnoEvent::SavingWorkspace:
		SaveLocation();
		break;

	case UnoEvent::ClosingDown:
		FormClosed();
		break;

	default:
		break;
	}
}

// Required to make sure the plugin is correctly unloaded when closed
void SDRunoPlugin_MemoPadUi::FormClosed()
{
	m_controller.RequestUnload(&m_parent);
}
