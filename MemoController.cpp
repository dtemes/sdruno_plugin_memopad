#include "MemoController.h"



MemoController::MemoController():
	frequency{ 0 },
	position(0)
{
	
}

int MemoController::Push(long long freq)
{
	//find hole
	long long f = frequency[position];
	int count = 0;
	while (f != 0 && count < NUM_MEMORIES + 1)
	{
		position++;
		position %= NUM_MEMORIES;
		f = frequency[position];
		count++;
	}

	frequency[position] = freq;

	/*
	while (count < NUM_MEMORIES - 1) 
	{
		long long tmp = frequency[pos];
		pos++;
		pos %= NUM_MEMORIES;
		frequency[pos] = tmp;
		count++;
	}*/
	
	return position;
}

long long MemoController::Pop()
{
	long long f = 0;
	int count = 0;
	while (f == 0 && count < NUM_MEMORIES)
	{
		position--;
		if (position < 0)
		{
			position = NUM_MEMORIES - 1;
		}
		f = frequency[position];
		count++;
	}
	return f;
}

long long MemoController::GetFrequency(int pos)
{
	pos %= NUM_MEMORIES;
	return frequency[pos];
}

int MemoController::SetPosition(int pos)
{
	position = pos;
	position %= NUM_MEMORIES;
	return position;
}

int MemoController::GetPosition()
{
	return position;
}

void MemoController::ClearAll()
{
	for (int i = 0; i < NUM_MEMORIES; i++)
	{
		frequency[i] = 0;
	}
}

void MemoController::ClearPosition(int i)
{
	frequency[i] = 0;
	
}

int MemoController::GetNumberOfMemories()
{
	return NUM_MEMORIES;
}