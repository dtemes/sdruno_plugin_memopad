#pragma once


#define NUM_MEMORIES 6

class MemoController
{

private:

	long long frequency[NUM_MEMORIES];
	int position;

public:
	MemoController();
	int GetNumberOfMemories();
	int Push(long long freq);
	long long Pop();
	long long GetFrequency(int pos);
	int SetPosition(int pos);
	int GetPosition();
	void ClearAll();
	void ClearPosition(int i);
};

