#include <sstream>
#ifdef _WIN32
#include <Windows.h>
#endif

#include "SDRunoPlugin_MemoPadSettingsDialog.h"
#include "SDRunoPlugin_MemoPadUi.h"
#include "resource.h"
#include <io.h>
#include <shlobj.h>

// Form constructor with handles to parent and uno controller - launches form TemplateForm
SDRunoPlugin_MemoPadSettingsDialog::SDRunoPlugin_MemoPadSettingsDialog(SDRunoPlugin_MemoPadUi& parent, IUnoPluginController& controller) :
	nana::form(nana::API::make_center(dialogFormWidth, dialogFormHeight), nana::appearance(true, false, true, false, false, false, false)),
	m_parent(parent),
	m_controller(controller)
{
	Setup();	
}

// Form deconstructor
SDRunoPlugin_MemoPadSettingsDialog::~SDRunoPlugin_MemoPadSettingsDialog()
{
	// **This Should not be necessary, but just in case - we are going to remove all event handlers
	// previously assigned to the "destroy" event to avoid memory leaks;
	this->events().destroy.clear();
}

// Start Form and start Nana UI processing
void SDRunoPlugin_MemoPadSettingsDialog::Run()
{	
	show();
	nana::exec();
}

int SDRunoPlugin_MemoPadSettingsDialog::LoadX()
{
	std::string tmp;
	m_controller.GetConfigurationKey("MemoPadSettings.X", tmp);
	if (tmp.empty())
	{
		return -1;
	}
	return stoi(tmp);
}

// Load Y from the ini file (if exists)
int SDRunoPlugin_MemoPadSettingsDialog::LoadY()
{
	std::string tmp;
	m_controller.GetConfigurationKey("MemoPadSettings.Y", tmp);
	if (tmp.empty())
	{
		return -1;
	}
	return stoi(tmp);
}

// Create the settings dialog form
void SDRunoPlugin_MemoPadSettingsDialog::Setup()
{
	// TODO: Form code starts here

	// Load X and Y locations for the dialog from the ini file (if exists)
	int posX = LoadX();
	int posY = LoadY();
	move(posX, posY);

	// This code sets the plugin size and title
	size(nana::size(dialogFormWidth, dialogFormHeight));
	caption("SDRuno Plugin MemoPad - Settings");

	// Set the forms back color to black to match SDRuno's settings dialogs
	this->bgcolor(nana::colors::black);

	// TODO: Extra form code goes here	
}